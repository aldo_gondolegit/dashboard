-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Jul 2020 pada 10.36
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `karyawan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `daftarkaryawan`
--

CREATE TABLE `daftarkaryawan` (
  `id` int(11) NOT NULL,
  `nik` int(11) NOT NULL,
  `agama` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `jk` varchar(255) DEFAULT NULL,
  `kewarganegaraan` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `ttl` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `daftarkaryawan`
--

INSERT INTO `daftarkaryawan` (`id`, `nik`, `agama`, `alamat`, `jk`, `kewarganegaraan`, `name`, `ttl`) VALUES
(16, 20177566, 'kristen', 'jl elok', 'laki-laki', 'WNI', 'KAK rendy', 'Jakarta, 20 juli 1999'),
(15, 20177566, 'kristen', 'jalan salak', 'laki-laki', 'WNI', 'shilvy', 'Jakarta, 20 Agustus 2002'),
(14, 20000, 'Kristen', 'Jalan Lebong', 'Perempuan', 'WNI', 'Fenny', 'Bengkulu, 10 Agustus 1990'),
(10, 6666, 'Kristen', 'JAKUT', 'laki-laki', 'WNI', 'ADIT', 'jakarta, 20 feb 2000'),
(13, 61762, 'islam', 'JALAN', 'laki-laki', 'WNI', 'Kak Wawan', 'Cirebon, 30 Maret 1990'),
(11, 20177304, 'Islam', 'Jalan Sama Aku, Nikah Sama Dia', 'Laki - Laki', 'WNI', 'ALDO', 'Jakarta, 20 Agustus 2002'),
(24, 4444444, 'budha', 'Jalan Krukut', 'laki-laki', 'WNI', 'dina', 'Jakarta, 20 juli 2002'),
(25, 6666666, 'budha', 'jl elok', 'laki-laki', 'WNI', 'anas', 'Jakarta, 20 juli 2002'),
(26, 20177566, 'islam', 'jalan salak', 'laki-laki', 'WNI', 'sonny', 'qwergh');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(7);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `daftarkaryawan`
--
ALTER TABLE `daftarkaryawan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `daftarkaryawan`
--
ALTER TABLE `daftarkaryawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
